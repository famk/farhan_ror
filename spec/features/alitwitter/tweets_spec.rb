require 'rails_helper'

RSpec.feature 'Alitwitter::Tweets', type: :feature do
  context 'create new tweet' do
    before(:each) do
      visit new_alitwitter_tweet_path
      within('form') do
      end
    end

    context 'given tweet exceeded 140 characters' do
      scenario 'should fail' do
        input = 'Test tweet'
        20.times do
          input << 'Test tweet'
        end
        fill_in 'Body', with: input
        click_button 'Post Tweet'
        expect(page).to have_content('Add Tweet')
      end
    end

    context 'given tweet exceeded with zero characters' do
      scenario 'should fail' do
        input = ''
        fill_in 'Body', with: input
        click_button 'Post Tweet'
        expect(page).to have_content('Add Tweet')
      end
    end

    context 'given tweet with valid number of characters' do
      scenario 'should success' do
        input = 'valid body'
        fill_in 'Body', with: input
        click_button 'Post Tweet'
        expect(page).to have_content('valid body')
      end
    end
  end
end
