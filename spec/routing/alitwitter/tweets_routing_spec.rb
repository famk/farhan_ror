# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Alitwitter::TweetsController, type: :routing do
  describe 'routing' do
    it 'routes to #index' do
      expect(get: '/alitwitter/tweets').to route_to('alitwitter/tweets#index')
    end

    it 'routes to #new' do
      expect(get: '/alitwitter/tweets/new').to route_to('alitwitter/tweets#new')
    end

    it 'routes to #create' do
      expect(post: '/alitwitter/tweets').to route_to('alitwitter/tweets#create')
    end

    it 'routes to #show' do
      expect(get: 'alitwitter/tweets/1').to route_to('alitwitter/tweets#show', id: '1')
    end

    it 'routes to #destroy' do
      expect(delete: 'alitwitter/tweets/1').to route_to('alitwitter/tweets#destroy', id: '1')
    end
  end
end
