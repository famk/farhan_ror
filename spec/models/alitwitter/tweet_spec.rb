# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Alitwitter::Tweet, type: :model do
  it 'is not valid without a body' do
    tweet = Alitwitter::Tweet.new(body: nil)
    expect(tweet).to_not be_valid
  end

  it 'is not valid when body is greater than 140 character' do
    temp = 'wkaodkoawkdoawkdoakwdoakwodakwodkaowkdaowkdaowkdoawkdoaw'
    temp += 'wkaodkoawkdoawkdoakwdoakwodakwodkaowkdaowkdaowkdoawkdoaw'
    temp += 'okoawkdoakwodakwodkawodkaowdk'
    tweet = Alitwitter::Tweet.new(body: temp)
    expect(tweet).to_not be_valid
  end
end
