# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'alitwitter/tweets/new', type: :view do
  it 'renders new tweets form' do
    render
    expect(rendered).to include('Add Tweet')
  end
end
