# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Alitwitter::TweetsController, type: :controller do
  describe 'GET #index' do
    it 'returns a success response' do
      get :index
      expect(response).to be_successful
    end
  end

  describe 'GET #new' do
    it 'returns a success response' do
      get :new
      expect(response).to be_successful
    end
  end

  describe 'GET #show' do
    it 'returns a success response' do
      get :new
      expect(response).to be_successful
    end
  end

  describe 'POST #create' do
    context 'with valid params' do
      it 'creates a new Tweet' do
        expect do
          post :create, params: { 'tweet' => { 'body' => 'valid body' } }
        end.to change(Alitwitter::Tweet, :count).by(1)
      end

      it 'redirects to the created Tweet' do
        post :create, params: { 'tweet' => { 'body' => 'valid body' } }
        expect(response).to redirect_to(Alitwitter::Tweet.last)
      end
    end

    context 'with invalid params' do
      it "returns a success response (i.e. to display the 'new' template)" do
        post :create, params: { 'tweet' => { 'body' => '' } }
        expect(response).to be_successful
      end
    end
  end

  describe 'DELETE #destroy' do
    it 'destroy the requested tweet' do
      tweet = Alitwitter::Tweet.create(body: 'valid body')
      expect do
        delete :destroy, params: { id: tweet.to_param }
      end.to change(Alitwitter::Tweet, :count).by(-1)
    end

    it 'redirects to the tweets list' do
      tweet = Alitwitter::Tweet.create(body: 'valid body')
      delete :destroy, params: { id: tweet.to_param }
      expect(response).to redirect_to(alitwitter_tweets_path)
    end
  end
end
