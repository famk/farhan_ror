# frozen_string_literal: true

Rails.application.routes.draw do
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html

  namespace :alitwitter do
    resources :tweets, only: %i[index new create show destroy]
  end
end
