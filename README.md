# Ali Twitter on Virtual Machines
A simple twitter application made for Ali

As a user,
- I want to be able to create a tweet, so that I can express what is on my mind.
- I want to be able delete a tweet, so that I can remove a tweet that I don't want to.
- I want to be able to see my tweets, so that I can read tweet that I have created before.

## Prerequisites

- Ruby 2.6.5
- Bundler 2.1.4
- Vagrant 2.2.7
- Virtualbox 6.1.4
- Ansible 2.9.6

## Introduction
This app is deployed on virtual machine that is provisioned with vagrant and installed and deployed using ansible 

## Architecture
### App
This app is deployed on two vm (`[app]192.168.50.4` and `[app2]192.168.50.6`) which use [vagrant forwarded port](https://www.vagrantup.com/docs/networking/forwarded_ports.html) which can be accessed from `localhost` host from port 3000 and 3001 respectively. Alternatively, the app can be accessed directly using its ip and port (`192.168.50.4:3000` for app and `192.168.50.6:3000` for app2).

### Database
This app use postgresql as database. The database is deployed on its own vm (`[db]192.168.50.5`). The database is configured with ansible to [accept remote connection](https://blog.bigbinary.com/2016/01/23/configure-postgresql-to-allow-remote-connection.html).

### Load balancer (HAProxy)
This app use HAProxy as load balancer. The load balancer is provisioned at its own vm using vagrant (`[lb]192.168.50.7`). The load balancer also use [vagrant forwarded port](https://www.vagrantup.com/docs/networking/forwarded_ports.html) so you can access the load balancer from host (`localhost:6000`) or use its own ip (`192.168.50.7`). 
The load balancer use `round-robin` so the request will be forwarded alternately between app vm.

### Gitlab Runner
This app support gitlab CICD pipeline. for CICD purposes, this app use local runner that is provisioned on its own vm using vagrant (`[runner]192.168.50.8`).

## Provision the vm 
Use `Vagrantfile` to provision:
```bash
$ vagrant up
```
## Setup all the vm
after provision the vm, you can setup all the vm.
```bash
$ ansible-playbook -i provisioning/hosts/hosts provisioning/install.yml
```
### Things that will be setup
You can see all things that will be setup on `provisioning/install.yml`
- Ruby installation (`provisioning/roles/install_ruby`). Installed on `app`, `app2`, and `gitlab-runner`.
- Nodejs installation (`provisioning/roles/install_nodejs`). Installed on `app`, `app2`, and `gitlab-runner`.
- Postgresql installation (`provisioning/roles/install_postgresql`). Installed on `db`.
- Database Creation (`provisioning/roles/create_db`). Created on `db`.
- Haproxy installation (`provisioning/roles/install_haproxy`). Installed on `lb`.
- Gitlab runner installation (`provisioning/roles/install_gitlab_runner`). Installed on `runner`.
- Ansible installation (`provisioning/roles/install_ansible`). Installed on `runner`.

## Artifact
artifact for this app is zip. in order to build the artifact you have to build the app:
```ruby
$ yarn 
$ bundle install
$ bundle exec rails assets:precompile RAILS_ENV=production
$ zip -r provisioning/roles/deploy_app/files/artifact_ali_twitter.zip .
```
## Deploy the artifact using ansible
after building the artifact, you can deploy the app using ansible.
```bash
$ ansible-playbook -i provisioning/hosts/hosts provisioning/deploy.yml
```
You can access the app on `192.168.50.7/alitwitter/tweets`
## Ansible variables

## Parameters
The following tables lists the configurable parameters of the Ansible roles and their default values.

Parameter                           | Description                                               | Default
----------------------------------- | --------------------------------------------------------- | -------------
`postgresql.database`                      | Database name                                 | `twitter`
`postgresql.username`                  | Database username                              | `alitwitter`
`postgresql.password`                         | Database password                                    | `alitwitter`
`artifact.name`                  | Artifact name                                          | `artifact_ali_twitter.zip`
`gitlab_runner.url`                      | Gitlab runner url                                               | `https://gitlab.com/`
`gitlab_runner.registration_token`                      | Gitlab runner registration token                                              | `YOUR TOKEN`
`gitlab_runner.shell.description`                  | Description for shell executor                                           | `shell-runner` 
`gitlab_runner.shell.tags`           | Tags for shell executor  | `shell-runner`
`os_environment.RAILS_ENV`          | Rails env                                          | `production`
`os_environment.RAILS_SERVE_STATIC_FILES`          | Rails serve static files (For prod)                                          | `true`
`os_environment.EXECJS_RUNTIME`                | Rails execjs runtime (For prod)                                     | `Disabled`
`os_environment.ALITWITTER_DATABASE_USERNAME`              | ALITWITTER_DATABASE_USERNAME env (db username)                                      | `alitwitter`
`os_environment.ALITWITTER_DATABASE_PASSWORD`       | ALITWITTER_DATABASE_USERNAME env (db password)                 | `alitwitter`
`os_environment.ALITWITTER_DATABASE_HOST`              | ALITWITTER_DATABASE_HOST env (db host / db vm ip)                        | `192.168.50.5`
`os_environment.ALITWITTER_DATABASE_PORT`        | ALITWITTER_DATABASE_PORT env (db port)                  | `5432`
`os_environment.ALITWITTER_DATABASE_DEVELOPMENT`                        | ALITWITTER_DATABASE_DEVELOPMENT env (db name for development)                                 | `twitter`
`os_environment.ALITWITTER_DATABASE_PRODUCTION`              | ALITWITTER_DATABASE_PRODUCTION env (db name for production)                           | `twitter`
`os_environment.secret_key_base`              | secret_key_base env (Secret for production)                           | `wkwk`

## CI/CD
to enable gitlab ci cd, you can change `gitlab_runner.registration_token` variables in `provisioning/group_vars/all.yml` according to your gitlab token.

In `Deploy` phase, the deployment use `gitlab-runner vm` and `gitlab-runner` as user. In order to make ansible work passwordless you have to generate ssh keygen in `gitlab-runner vm` as `gitlab-runner` user to `app` and `app2` vms.
```bash
$ ssh vagrant runner
[vagrant@runner]$ sudo su
[root@runner]$ su gitlab-runner
[gitlab-runner@runner]$ ssh-keygen
# app vm
[gitlab-runner@runner]$ ssh-copy-id vagrant@192.168.50.4 
# app2 vm
[gitlab-runner@runner]$ ssh-copy-id vagrant@192.168.50.6

```
