# frozen_string_literal: true

module Alitwitter
  def self.table_name_prefix
    'alitwitter_'
  end
end
