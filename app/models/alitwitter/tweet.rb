# frozen_string_literal: true

module Alitwitter
  class Tweet < ApplicationRecord
    validates :body, presence: true,
                     length: { maximum: 140 }
  end
end
