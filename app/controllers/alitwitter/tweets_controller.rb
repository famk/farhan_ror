# frozen_string_literal: true

module Alitwitter
  class TweetsController < ApplicationController
    def index
      @tweets = Tweet.all
    end

    def new; end

    def show
      @tweet = Tweet.find(params[:id])
    end

    def create
      @tweet = Tweet.new(tweet_params)
      if @tweet.save
        redirect_to @tweet
      else
        render 'new'
      end
    end

    def destroy
      @tweet = Tweet.find(params[:id])
      @tweet.destroy

      redirect_to alitwitter_tweets_path
    end

    private
    def tweet_params
      params.require(:tweet).permit(:body)
    end
  end
end
